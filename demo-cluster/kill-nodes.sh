#!/bin/sh
set -e

containers="$(docker ps -a | egrep '\s+redis-[0-9]+' | cut -d' ' -f1 | xargs)"
[ -z "$containers" ] && exit 0
echo "Stopping"
docker stop -t 3 $containers
echo "Removing"
docker rm $containers
