#!/bin/sh
set -e

PORT=6379

if [ $(id -u) -ne 0 ]; then
	echo "This script must be run as root"
	exit 1
fi

masters=$1
slaves=$2
if [ -z $masters -o -z $slaves ]; then
	echo "Usage: $0 <master node count> <slave node count per master>"
	exit 2
fi
if [ $masters -lt 1 ]; then
	echo "master node count must be >= 1"
	exit 3
fi
if [ $slaves -lt 1 ]; then
	echo "slave node count must be >= 1"
	exit 3
fi

total=$(expr $masters + $masters \* $slaves)
cmdline=""

echo "Launching $total nodes"

for i in $(seq 1 $total); do
	echo "Starting instance $i"
	cid=$(docker run -ti --detach=true --name="redis-$i" distribue-redis)
	cip=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' "$cid")
	echo "  Instance $i: $cid $cip"
	cmdline="$cmdline $cip:$PORT"
done

echo "Done. Command line is:"
echo
echo "    ./redis-trib.rb create --replicas $slaves$cmdline"
echo
